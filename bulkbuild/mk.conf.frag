PKG_DEVELOPER=yes

MAKE_JOBS=3
SKIP_LICENSE_CHECK=yes
PKG_COMPILER=ccache gcc
ALLOW_VULNERABLE_PACKAGES=YES
USE_CWRAPPERS=yes

WRKOBJDIR=              /tmp
PACKAGES=               /srv/packages
DISTDIR=                /srv/distfiles

PKG_DEFAULT_OPTIONS+= ncursesw
